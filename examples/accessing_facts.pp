notify { 'notice 1':
    # $facts['networking']['ip']
    message => "${facts['networking']['ip']}",
}

notify { 'notice 2':
    # $::networking['ip']
    message => "${::networking['ip']}",
}
