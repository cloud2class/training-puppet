# security_profiles manifest
class security_profiles (
  $manage_ssl = false
) {

  notify { 'Not secure': }

}

class security_profiles::accept {
  notify { 'Secured': }
}

class { 'security_profiles':
  manage_ssl => true
}
