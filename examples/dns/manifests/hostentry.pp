define dns::hostentry (
  String $entry,
) { 

  file_line {'Adding entries to /etc/hosts':
    path   => '/etc/hosts',
    ensure => present,
    line   => $entry,
  }
}
