def check_network(network)
    case network
        when "188.166.144.0"
            result = "Paris"
        when "10.131.0.0"
            result = "London"
        when "178.62.64.0"
            result = "Berlin"
        else
            result = "Madrid"
    end
    return result
end

Facter.add("datacenter") do
    setcode do
        location = "Unknown"
        interfaces = Facter.value(:interfaces)
        interfaces.split(',').each do |interface| 
            subnet = Facter.value("network_#{interface}")
            sublocation = check_network(subnet)
            if sublocation != "Unknown"
                location = sublocation
            end
        end
        location
    end
end

