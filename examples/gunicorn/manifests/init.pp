# Class: gunicorn
#
# This class installs gunicorn
class gunicorn{

  package { 'python-setuptools':
    ensure => latest,
  }
  exec { '/usr/local/bin/easy_install gunicorn':
    creates => '/usr/bin/gunicorn',
  }
  file { '/usr/bin/later':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0744',
    content => 'echo "See you later"',
  }
}
