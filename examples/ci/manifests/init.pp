# Class: ci
# ===========================
#
# Full description of class ci here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'ci':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class ci {
    package { 'openjdk-8-jre-headless':
        ensure => latest,
    }

    file { 'get jenkins key':
        ensure => present,
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        path   => '/var/tmp/jenkins-ci.org.key',
        source => 'https://pkg.jenkins.io/debian/jenkins-ci.org.key',
        notify => Exec['add jenkins key'],
    }

    exec { 'add jenkins key':
        command     => '/usr/bin/apt-key add /var/tmp/jenkins-ci.org.key',
        refreshonly => true,
    }

    file { '/etc/apt/sources.list.d/jenkins.list':
        ensure  => present,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => 'deb http://pkg.jenkins.io/debian-stable binary/',
        notify  => Exec['refresh apt database'],
    }

    exec { 'refresh apt database':
        command     => '/usr/bin/apt-get update',
        refreshonly => true,
    }

    package { 'jenkins':
        ensure => latest,
    }

    service { 'jenkins':
        ensure => running,
        enable => true,
    }
}
