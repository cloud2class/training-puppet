# Class: webservice
#
# This class installs a demo webservice
class webservice {

  file { 'source':
    path   => '/tmp/source.c',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/webservice/sourcefile'
  }

  exec { '/usr/bin/gcc -Wall /tmp/source.c -o /usr/bin/hello':
    creates => '/usr/bin/hello',
    require => File['source'],
  }

  file { '/usr/bin/later':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0744',
    content => 'echo "See you later"',
  }
}
