# Example class to install and start the Redis service

class install_redis {

  package { 'redis':
    ensure => latest,
  }

  service { 'redis':
    ensure => running,
    enable => true,
  }

  file { '/etc/redis.conf':
    ensure => present,
    source => '/root/redis-puppet.conf',
  }

}
