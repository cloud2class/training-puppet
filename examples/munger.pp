class munger (
  $input = 'false'
) {
  if $input == true {
    notify { 'Correct': }
  }
  else {
    notify { 'Didn\'t receive a Boolean': }
  }
}

class { 'munger':
  input => 'true'
}
