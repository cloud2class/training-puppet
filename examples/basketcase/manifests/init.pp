class basketcase {
    $port = 8080
    $bind = '0.0.0.0'
    $tcpkeepalive = 60
    $pidfile = '/var/run/redis/redis-server.pid'
    $logfile = '/var/log/redis/redis-server.log'

    exec { 'Adding Epel repo':
        cmd =>'/bin/rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm',
        creates => '/etc/yum.repos.d/epel.repo',
    }

    package { 'redis':
        ensure  => installed,
        require => Exec['Adding Epel repo']
    }

    service { 'redis':
        ensure  => 'running',
        require => Package['redis'],
    }

    file { '/etc/redis/redis.conf':
        ensure  => present,
        owner   => 'root',
        group   => 'root',
        mode    => '0640',
        content => template('basketcase/redis.conf.erb'),
        require => Package['redis'],
        notify  => Service['redis'],
    }
}
