# Class: tower
class tower {

  if $::var_top {
    notify { "Top Scope variable is: ${var_top}": }
  }

  contain tower::library
  contain tower::townhall

}
