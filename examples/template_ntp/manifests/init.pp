# template_ntp class
class template_ntp (
  $servers = ['0.centos.pool.ntp.org',
              '1.centos.pool.ntp.org',
              '2.centos.pool.ntp.org',
              '3.centos.pool.ntp.org']
) {

package {'ntp':
  ensure => installed,
}

service { 'ntpd':
  ensure => running,
  enable => true,
  require => Package['ntp']
}

file {'/etc/ntp.conf':
  ensure => present,
  owner  => 'root',
  group  => 'root',
  source => 'puppet:///modules/template_ntp/ntp.conf'
}


}
