$content = "# Warning. This file is managed by Puppet" 
 
file { '/home/access/boilerplate': 
  ensure => present, 
  content => '# Simple Warning'
}
 
file_line { 'add boilerplate to file': 
  ensure => present, 
  path => '/home/access/boilerplate',
  match => '# Simple Warning', 
  line => $content 
}
