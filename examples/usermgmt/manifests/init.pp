# == Class: usermgmt
#
# This module creates users groups from a hiera hashes
#
# === Security
#
# This module can be used to appy the following security recommendation.
# Block shell and login access for all non-root system accounts.
# Reference: CIS: 9.1, NSA: 2.3.1.4
#
# === Parameters
#
# [*userhash*]
#   This is a hash of users passed to the module
# [*enable_user_clean*]
#   This paramater will cause the deletion of any users not explicily defined.
#   This is done by combining two hashes, one is defined by the user the other
#   is a cutsom fact.  The combination of these two hashes produces a definitive
#   list of what users are required on the system.
#   Put simply any user on a server that is not in the userhash list will be set
#   as absent, including system users.
#
# === Variables
#
# [*userlist*]
#  This is a hash from hiera that is used if the userhash list is not set
#
# === Examples
#
# [*hiera*]
#  classes: - 'usermgmt'
#  usermgmt::userlist:
#    'someuser':
#      shell: '/sbin/nologin'
#      uid: '400'
#      ssh_keys:
#        'mykey':
#          type: 'ssh-rsa'
#          key:  'mykeydata=='
#  usermgmt::group:
#    'somegroup':
#      gid: '3000'
#
class usermgmt (
  $enable_user_clean = false,
  $userhash          = undef,
  $grouphash         = undef,
  $disabled          = false,
) {

  if $disabled {
    $userlist  = undef
    $grouplist = undef
  } else {
    $userlist  = $userhash
    $grouplist = $grouphash
  }

  unless $grouplist == undef {

    validate_hash($grouplist)
    create_resources('usermgmt::group', $grouplist)

  }

  unless $userlist == undef {
    validate_hash($userlist)
    if $enable_user_clean {
      validate_hash($::user_absent)
      $complete_userlist = merge($::user_absent,$userlist)
    } else {
      $complete_userlist = $userlist
    }

    #This dependency is automatically met on older releases
    if ($::lsbdistcodename == 'xenial') {
        $libshadow_require = Package['ruby-dev']
    }
    package { 'libshadow':
      ensure   => present,
      provider => gem,
      require  => $libshadow_require,
    }

    validate_hash($complete_userlist)
    create_resources('usermgmt::user', $complete_userlist)

  }

}
