require_relative '../../../../spec_helper'

describe 'usermgmt::group', :type => :define do

  context 'with explicit data' do
    let(:title) { 'testgroup' }

    let(:params) {{
      :gid => '1111'
    }}

    it do
      should contain_group('testgroup').with(
        'ensure' => 'present',
      )
    end

  end
end

