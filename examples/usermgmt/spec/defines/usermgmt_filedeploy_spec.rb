require_relative '../../../../spec_helper'

describe 'usermgmt::filedeploy', :type => :define do

  context 'with explicit data' do
    let(:title) { 'testfiledeploy' }

    let(:params) {{
      :owner => 'testuser',
      :group => 'testgroup',
      :content => 'stuff',
      :requirement => 'File["/something"]',
    }}

    it do
      should contain_file('testfiledeploy').with(
        'ensure'  => 'present',
        'owner'   => 'testuser',
        'group'   => 'testgroup',
        'content' => 'stuff',
        'require' => 'File["/something"]',
      )
    end

  end
end

