require_relative '../../../../spec_helper'

describe 'usermgmt', :type => :class do

  context 'with explicit data' do
    let(:facts) {{ :user_absent => {} }}

    let(:params) {{
      :userhash => {
        'testuser' => { 'fullname' => 'Test User',
                        'email'    => 'test@test.test',
                        'ssh_pub_keys' => {
                          'type' => 'ssh-rsa',
                          'key'  => 'testkey',
                        },
                        'groups' => 'testgroup',
                        'ssh_private_keys' => {
                          '/home/testuser/.ssh/test_private_key' => {
                            'content' => 'test-key',
                          }
                        },
                        'home_dir_files' => {
                          '/home/testuser/.special_test_file' => {
                            'content' => 'test test',
                          },
                        }
        }
      },
      :grouphash => {
        'testgroup' => {},
      }
    }}


  it { should contain_package('libshadow').with({ 'ensure' => 'present',  })}

  end
end

