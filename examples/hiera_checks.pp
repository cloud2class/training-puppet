# An example class to demonstrate hiera calls
# puppet apply --hiera_config=./hiera.yaml examples/hiera_checks.pp

$task_1_data = hiera('noquery', 'default')
$task_2_data = hiera('noquery', 'default')
$task_3_data = hiera('noquery', 'default')
$task_4_data = hiera('noquery', 'default')

notify { 'task 1':
    message => $task_1_data
}
notify { 'task 2':
    message => $task_2_data
}
notify { 'task 3':
    message => $task_3_data
}
notify { 'task 4':
    message => $task_4_data
}
