class badcoding {

  # What we would like to manage
  #  whether the rsyncd service is running or not
  #  whether the rsyncd service is enabled or not
  #  chroot or not
  #  max connections
  #  timeout

  $chroot = false
  $max_connections = 2
  $timeout = 60

  package {'rsync':
    ensure => present,
  }

  service {'rsyncd':
    ensure  => running,
    enable  => true,
  }

  file { '/etc/rsyncd.conf':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('badcoding/rsyncd.conf.erb'),
    notify  => Service['rsyncd'],
  }
}
